package chatClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import utils.Validations.ValidateUserName;

public class Client {
    public static void main(String[] args) throws IOException {

        ValidateUserName validateUserName = new ValidateUserName();
        if((validateUserName.validateSender() && validateUserName.validateReceiver())) {
            Socket socket = new Socket("127.0.0.1", 8765);
            BufferedReader serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter serverOut = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));
            String line;
            while (!"exit".equalsIgnoreCase(line = br.readLine())) {
                serverOut.println(line);
                String response =  serverIn.readLine();
                System.out.println("Client got this respone : " + response);
            }
            serverOut.println("exit");
            socket.close();
        }
        

    }
}
