# Client.java
> Establishes a connection with the server on a specific hostname and port.
> Provides methods for sending and receiving messages (encryption on sending, decryption on receiving).