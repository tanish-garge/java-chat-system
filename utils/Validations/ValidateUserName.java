package utils.Validations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import chatServer.Users;

public class ValidateUserName {

    public boolean validateSender() throws IOException{
        System.out.print("Your Username: ");
        BufferedReader readUsername = new BufferedReader(new InputStreamReader(System.in));
        String sender = readUsername.readLine();
        
        if(Users.isPresent(sender)) {
            System.out.println(sender + " is Authenticated");
            return true;
        } else {
            System.out.println(sender + " is not Authenticated");
            return false;
        }
    }

    public boolean validateReceiver() throws IOException {
        System.out.print("Receiver's Username: ");
        BufferedReader readReceiverNamer = new BufferedReader(new InputStreamReader(System.in));
        String receiver = readReceiverNamer.readLine();

        if(Users.isPresent(receiver)) {
            System.out.println(receiver + " is Authenticated");
            return true;
        } else {
            System.out.println(receiver + " is not Authenticated");
            return false;
        }


    }
}
