package chatServer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class Users {
    public static final List<String> USERNAMES = Collections.unmodifiableList(
            Arrays.asList("Tanish", "Aai", "Baba"));

    public static boolean isPresent(String userName) {
        if(USERNAMES.contains(userName)) {
            return true;
        }
        return false;
    }

}
