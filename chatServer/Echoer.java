package chatServer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Echoer extends Thread{

    private Socket clientSocket;

    public Echoer(Socket clientSocket) {
        this.clientSocket = clientSocket;
        this.start();
    }

    public void run() {
        try{
            BufferedReader clientIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter clientOut = new PrintWriter(clientSocket.getOutputStream(), true);

            String line;
            while (!"exit".equalsIgnoreCase((line = clientIn.readLine()))) {
                System.out.println("Server Got: " + line);
                clientOut.println("Server Got: " + line);
            }
            System.out.println("Client Closed");
            clientSocket.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
}