# Server.java
> Creates a **ServerSocket** on a specific port.
> Accepts client connections and creates new threads for each.
> Handles message receiving, decryption, routing, and encryption of outgoing messages.
> Manages a list of connected clients and their usernames.
> Offers methods for adding/removing clients and broadcasting messages.

ClientThread.java

# Message.java
> Contains fields like sender, recipient, content, timestamp, etc.
